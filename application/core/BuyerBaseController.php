<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BuyerBaseController extends MY_Controller{

  const THEMEDIR = "buyer/themes/";

  public $alias = 'home';
  public $themeDir = "buyer/themes/default";
  public $SITE;

  function __construct(){
    parent::__construct();
    $this->load->library('doctrine');
    $this->em = $this->doctrine->em;
    $this->load->model('site');

    $domain = $this->parseDomain();
    $this->SITE = $this->site->getOne($domain);

    $this->themeDir = self::THEMEDIR . $this->SITE->theme->directory . "/";

  }

  public function loadView($page, $data = array()){
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      // header('Content-Type: application/json');
      // echo json_encode($data);
    }else{
      
      if(!$this->SITE){
        $this->load->view('404');
        return;
      }

      $this->buildScripts();
      die();

      $masterPage =  $this->themeDir . "layout";

      $_data = $data;

      $_data['alias'] = $this->alias;
      
      $_data['title'] = isset($data['title'])?$data['title']:$this->SITE->theme->default_title;

      $_data['page'] = $this->themeDir . "pages/" . $page;


      $this->load->view($masterPage, $_data);
    }
    $this->em->getConnection()->close();
  }
  public function parseDomain(){
    return $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'];
  }
  public function buildScripts(){
    print_r(parse_ini_file(APPPATH."views/" . $this->themeDir . "scripts.php"));
    // $scripts =$this->load->view($themeDir . "scripts");
  }
  public function buildStyles(){
    
  }
  public function assets($file){
    header('Content-Type: ' . mime_content_type($file));
    $this->readfile_chunked($file);
    // readfile($file);
  }

  // Read a file and display its content chunk by chunk
  public function readfile_chunked($filename, $chunk = 1048576, $retbytes = TRUE) { // 1024*1024
    $buffer = '';
    $cnt    = 0;
    $handle = fopen($filename, 'rb');

    if ($handle === false) {
        return false;
    }

    while (!feof($handle)) {
        $buffer = fread($handle, $chunk);
        echo $buffer;
        ob_flush();
        flush();

        if ($retbytes) {
            $cnt += strlen($buffer);
        }
    }

    $status = fclose($handle);

    if ($retbytes && $status) {
        return $cnt; // return num. bytes delivered like readfile() does.
    }

    return $status;
  }
  public function _404(){
    $this->load->view('404');
  }
}